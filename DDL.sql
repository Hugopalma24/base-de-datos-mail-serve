CREATE TABLE user(
	userId INTEGER PRIMARY KEY,
	userName VARCHAR(100),
	userPassword VARCHAR(100),
	userEmail VARCHAR(100)
);

CREATE TABLE server(
	serverId INTEGER PRIMARY KEY,
	ip VARCHAR(100),
	serverName VARCHAR(100)
);

CREATE TABLE mail(
	mailId INTEGER PRIMARY KEY,
	mailSender VARCHAR(100),
	mailSubject VARCHAR(100),
	mailBody VARCHAR(100),
	mailIdUser INTEGER,
	FOREIGN KEY(mailIdUser) REFERENCES user(userId)
);

CREATE TABLE  contact(
	userIdOwner INTEGER,
	userIdContact INTEGER
);
